/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvcstudent;

/**
 *
 * @author HP
 */
public class Controller {
    Student model;
    StudentView view;
    
    public Controller(Student model, StudentView view){
        this.model=model;
        this.view=view;
    }
    public void updateView(){
        view.printStudentDetails(model);
    }
}
